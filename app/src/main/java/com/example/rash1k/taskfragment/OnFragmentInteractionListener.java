package com.example.rash1k.taskfragment;

import android.net.Uri;

public interface OnFragmentInteractionListener {
    void onFragmentInteraction(Uri uri);
    void onFragmentName(String nameFragment);
}
