package com.example.rash1k.taskfragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;

public class MainActivity extends AppCompatActivity implements OnFragmentInteractionListener {


    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    private CheckBox mRedCheckBox;
    private CheckBox mGreenCheckBox;
    private CheckBox mBlueCheckBox;

    private FragmentManager.OnBackStackChangedListener mListener = new FragmentManager.OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            int count = getSupportFragmentManager().getBackStackEntryCount();
            Log.d(LOG_TAG, "Count: " + count);
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < count; i++) {
                FragmentManager.BackStackEntry entry =
                        getSupportFragmentManager().getBackStackEntryAt(i);

                builder.append("name: ")
                        .append("\ntoString: ")
                        .append(entry.toString())
                        .append(entry.getName())
                        .append("\nId: ")
                        .append(entry.getId())
                        .append("\nBreadCrumbShortTitle: ")
                        .append(entry.getBreadCrumbShortTitle())
                        .append("\ngBreadCrumbTitle: ")
                        .append(entry.getBreadCrumbTitle());


            }
            Log.d(LOG_TAG, "onBackStackChanged: " + builder);
        }
    };

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRedCheckBox = (CheckBox) findViewById(R.id.checkbox_red);
        mGreenCheckBox = (CheckBox) findViewById(R.id.checkbox_green);
        mBlueCheckBox = (CheckBox) findViewById(R.id.checkbox_blue);

    }


    @Override
    protected void onStart() {
        super.onStart();
        getSupportFragmentManager().addOnBackStackChangedListener(mListener);
    }


    @Override
    protected void onPause() {
        super.onPause();
        getSupportFragmentManager().removeOnBackStackChangedListener(mListener);
    }


    public void onClick(View view) {
        final FragmentManager manager = getSupportFragmentManager();
        final FragmentTransaction transaction = manager.beginTransaction();
        Fragment fragment = null;
        int id = view.getId();
        if (((CheckBox) view).isChecked()) {
            switch (id) {
                case R.id.checkbox_red:
                    fragment = MainActivityFragmentRed.newInstance(null, null);
                    break;
                case R.id.checkbox_green:
                    fragment = MainActivityFragmentGreen.newInstance(null, null);
                    break;
                case R.id.checkbox_blue:
                    fragment = MainActivityFragmentBlue.newInstance(null, null);
                    break;
            }

            transaction.add(R.id.fragment_container, fragment)
                    .addToBackStack(null).commit();
        } else {
            manager.popBackStack();
           /* fragment = manager.findFragmentById(R.id.fragment_container);
            transaction.remove(fragment).commit();*/
        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onFragmentName(String nameFragment) {
        switch (nameFragment) {
            case "MainActivityFragmentRed":
                mRedCheckBox.setChecked(false);
                break;
            case "MainActivityFragmentGreen":
                mGreenCheckBox.setChecked(false);
                break;
            case "MainActivityFragmentBlue":
                mBlueCheckBox.setChecked(false);
                break;
        }
    }
}
